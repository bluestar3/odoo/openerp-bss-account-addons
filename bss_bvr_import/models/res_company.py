# -*- coding: utf-8 -*-
# Part of BVR Imports.
# See LICENSE file for full copyright and licensing details.

from datetime import date
import re

from odoo import api, fields, models
from odoo.tools.cache import ormcache

from odoo.addons.bss_utils.utils.dateutils import orm2date


class ResCompany(models.Model):
    _inherit = "res.company"

    RE_DIGITS = re.compile(r"\d+")

    DATE_FIELDS = (
        ('deposit_date', "Deposit Date"),
        ('treatment_date', "Treatment Date"),
        ('credit_date', "Credit Date"))

    esr_import_reconcile_margin = fields.Float("ESR Payment Import Margin",)
    esr_import_write_off_account_id = fields.Many2one(
        'account.account', "ESR Payment Import Write-Off Account")
    esr_import_date_field = fields.Selection(DATE_FIELDS, "ESR Date Field")
    esr_delta_horizontal = fields.Float(
        "ESR Horizontal Delta (mm)", (6, 2), default=0.0,
        help="Horizontal delta for the ESR print in mm. Positive value means "
        "translating to the right, negative value is possible")
    esr_delta_vertical = fields.Float(
        "ESR Vertical Delta (mm)", (6, 2), default=0.0,
        help="Vertical delta for the ESR print in mm. Positive value means "
        "translating up, negative value is possible")
    esr_bank_account_id = fields.Many2one(
        'account.journal', "ESR Bank", domain=[
            ('type', '=', 'bank'),
            ('inbound_payment_method_ids.code', '=', 'esr')])
    esr_partner_ref = fields.Many2one(
        'ir.model.fields', "ESR Partner Reference", domain=[
            ('model', '=', 'res.partner')])
    esr_invoice_ref = fields.Many2one(
        'ir.model.fields', "ESR Invoice Reference", domain=[
            ('model', '=', 'account.invoice')])
    esr_invoice_type = fields.Integer("ESR Invoice Type")
    esr_payment_ref_format = fields.Char(
        "ESR Payment Ref Format",
        default="{custom:0>8}{type:0>2}{partner:0>7}")

    @api.multi
    @ormcache('self.fiscalyear_lock_date', 'self.period_lock_date')
    def account_lock_date(self):
        self.ensure_one()
        return max(
            orm2date(
                self.fiscalyear_lock_date, default=date.min),
            orm2date(self.period_lock_date, default=date.min))

    @api.multi
    def _get_esr_partner_ref(self, obj):
        self.ensure_one()
        return int(u''.join(self.RE_DIGITS.findall(u"{}".format(getattr(
            obj, self.esr_partner_ref.name)))))

    @api.multi
    def _get_esr_invoice_ref(self, obj):
        self.ensure_one()
        return int(u''.join(self.RE_DIGITS.findall(u"{}".format(getattr(
            obj, self.esr_invoice_ref.name)))))
