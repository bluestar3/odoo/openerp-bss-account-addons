# -*- coding: utf-8 -*-
# Part of BVR Imports.
# See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api


class AccountConfigSettings(models.TransientModel):
    _inherit = 'account.config.settings'

    esr_import_reconcile_margin = fields.Float(
        "ESR Payment Import Margin",
        related='company_id.esr_import_reconcile_margin')
    esr_import_write_off_account_id = fields.Many2one(
        'account.account', "ESR Payment Import Write-Off Account",
        related='company_id.esr_import_write_off_account_id')
    esr_import_date_field = fields.Selection(
        lambda self: self.env['res.company'].DATE_FIELDS,
        "ESR Date Field", related='company_id.esr_import_date_field')
    esr_delta_horizontal = fields.Float(
        "ESR Horizontal Delta (mm)", (6, 2), default=0.0,
        help="Horizontal delta for the ESR print in mm. Positive value means "
        "translating to the right, negative value is possible",
        related='company_id.esr_delta_horizontal')
    esr_delta_vertical = fields.Float(
        "ESR Vertical Delta (mm)", (6, 2), default=0.0,
        help="Vertical delta for the ESR print in mm. Positive value means "
        "translating up, negative value is possible",
        related='company_id.esr_delta_vertical')
    esr_bank_account_id = fields.Many2one(
        'account.journal', "ESR Bank", domain=[
            ('type', '=', 'bank'),
            ('inbound_payment_method_ids.code', '=', 'esr')],
        related='company_id.esr_bank_account_id')
    esr_partner_ref = fields.Many2one(
        'ir.model.fields', "ESR Partner Reference", domain=[
            ('model', '=', 'res.partner')],
        related='company_id.esr_partner_ref')
    esr_invoice_ref = fields.Many2one(
        'ir.model.fields', "ESR Invoice Reference", domain=[
            ('model', '=', 'account.invoice')],
        related='company_id.esr_invoice_ref')
    esr_invoice_type = fields.Integer(
        "ESR Invoice Type", related='company_id.esr_invoice_type')
    esr_payment_ref_format = fields.Char(
        "ESR Payment Ref Format",
        related='company_id.esr_payment_ref_format')

    @api.depends()
    def _compute_esr_fields(self):
        for r in self:
            r.write(r._get_esr_fields())
