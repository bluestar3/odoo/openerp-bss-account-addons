# -*- coding: utf-8 -*-
# Part of BVR Imports.
# See LICENSE file for full copyright and licensing details.

from odoo import api, models


class ResPartner(models.Model):
    _inherit = "res.partner"

    @api.multi
    def _get_esr_partner_ref(self):
        self.ensure_one()
        return self.company_id._get_esr_partner_ref(self)
