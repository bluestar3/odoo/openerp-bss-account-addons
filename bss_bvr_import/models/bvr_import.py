# -*- coding: utf-8 -*-
# Part of BVR Imports.
# See LICENSE file for full copyright and licensing details.

import base64
from camt054_handler import Camt054Handler
import cStringIO
from datetime import datetime
from decimal import Decimal as D
import hashlib
import logging
from psycopg2.extensions import TransactionRollbackError
import threading
import time
import xml.sax

from odoo import api, models, fields, _
from odoo.exceptions import UserError

import odoo.addons.decimal_precision as dp  # @UnresolvedImport

from odoo.addons.bss_utils.utils.dateutils import (
    orm2datetime)  # @UnresolvedImport
from odoo.addons.bss_utils.utils.dateutils import orm2date


STATES = (('new', u"New"),
          ('queue_importation', u"Importation Queued"),
          ('importation', u"Importation"),
          ('imported', u"Imported"),
          ('queue_reconciliation', u"Reconciliation Queued"),
          ('reconciliation', u"Reconciliation"),
          ('terminated', u"Terminated"),
          ('failed', u"Failed"))

service_lock = threading.Lock()


class BvrImport(models.Model):
    _name = 'bss.bvr_import'
    _inherit = ['mail.thread', 'bss.abstract_queue']
    _description = "ESR Import"
    _logger = logging.getLogger(_name)
    _order = 'create_date desc'

    @api.model
    def _default_debit_account_id(self):
        account_id = None
        account_receivable = self.env['ir.property'].search(
            [('name', '=', 'property_account_receivable_id')], limit=1)
        if account_receivable:
            account_id = account_receivable.value_reference.split(',')[1]
        return account_id

    @api.model
    def _default_control_hash(self):
        return hashlib.sha256(u'').hexdigest()

    company_id = fields.Many2one(
        'res.company', "Company", required=True,
        default=lambda self:
            self.env['res.company']._company_default_get('bss.bvr_import'))
    journal_id = fields.Many2one(
        'account.journal', "Journal", required=True,
        domain=[('type', '=', 'bank'),
                ('inbound_payment_method_ids.code', '=', 'esr')])
    debit_account_id = fields.Many2one(
        'account.account', string="Debit Account",
        default=_default_debit_account_id)
    imported_file = fields.Binary(
        compute="_compute_imported_file", inverse="_inverse_imported_file",
        store=False, string="File", required=True)
    imported_file_checksum = fields.Char(
        "Imported File Checksum", readonly=True)
    state = fields.Selection(
        STATES, 'State', required=True, readonly=True, default="new")
    move_ids = fields.One2many(
        'account.move', 'bvr_import_id', "Account Moves")
    count_move_line = fields.Integer(
        "Move Lines", compute="_compute_counts", store=False)
    count_unreconciled_move_line = fields.Integer(
        "Unreconciled Move Lines", compute="_compute_counts", store=False)
    count_no_partner_move_line = fields.Integer(
        "No Debtor Move Lines", compute="_compute_counts", store=False)
    reconcile_margin = fields.Float(
        "Reconcile Margin", related='company_id.esr_import_reconcile_margin')
    write_off_account_id = fields.Many2one(
        'account.account', "Write-Off Account",
        related='company_id.esr_import_write_off_account_id')
    date_field = fields.Selection(
        lambda self: self.env['res.company'].DATE_FIELDS,
        "Date Field", related='company_id.esr_import_date_field')
    transaction_type = fields.Char("Transaction Type", size=3, readonly=True)
    client_reference = fields.Char("Client Reference", size=10, readonly=True)
    sort_key = fields.Char("Sort Key", size=28, readonly=True)
    amount = fields.Float(
        "Amount", digits=dp.get_precision('Account'), readonly=True)
    line_count = fields.Integer('Line Count', readonly=True)
    original_date = fields.Datetime("Original Date", readonly=True)
    payment_price_value = fields.Float(
        'Payment Price Value', digits=dp.get_precision('Account'),
        readonly=True)
    bvr_plus = fields.Char("BVR+", size=10, readonly=True)
    control_hash = fields.Char(
        "Control Hash", size=64, readonly=True, required=True,
        default=_default_control_hash)

    @api.model
    def lang_code(self):
        """Return the language code of the current user.
        Return an empty string if user lang is not defined.
        """
        code = self.env.context and self.env.context.get('lang')
        if not code:
            # If the lang is not in context, take the user lang :
            code = self.env.user.partner_id.lang
        return code

    @api.model
    def lang(self):
        """Return the language object of the current user"""
        code = self.lang_code()

        # If the lang is not defined in context and on current user,
        # take the first one :
        domain = []
        if code:
            domain = [('code', '=', code)]

        return self.env['res.lang'].search(domain)[0]

    @api.multi
    def name_get(self):
        res = []
        lang = self.lang()
        dt_format = "%s %s" % (lang.date_format, lang.time_format)
        for r in self:
            res.append((r.id, orm2datetime(
                r.create_date).strftime(dt_format)))
        return res

    @api.multi
    def _compute_imported_file(self):
        for record in self:
            attachment = self.env['ir.attachment'].search([
                ('res_model', '=', 'bss.bvr_import'),
                ('res_id', '=', record.id)
            ], limit=1)
            if attachment:
                record.imported_file = attachment[0].datas
                if not record.imported_file:
                    record.imported_file = u'IAo='
            else:
                record.imported_file = u'IAo='

    @api.multi
    def _inverse_imported_file(self):
        for record in self:
            self.env['ir.attachment'].sudo().create({
                'name': _("Imported File"),
                'datas_fname': _("Imported File"),
                'datas': record.imported_file,
                'res_model': 'bss.bvr_import',
                'res_id': record.id,
            })

    @api.multi
    def _compute_counts(self):
        cr = self.env.cr
        cr.execute("""
            select
                c.import_id,
                sum(c.move_line) count_move_line,
                sum(c.unreconciled_move_line) count_unreconciled_move_line,
                sum(c.no_partner_move_line) count_no_partner_move_line
            from (
                select
                    bi.id import_id,
                    1 move_line,
                    case when ml.reconciled = false
                        then 1 else 0 end unreconciled_move_line,
                    case when ml.partner_id is null
                        then 1 else 0 end no_partner_move_line
                from bss_bvr_import bi
                inner join account_move m on m.bvr_import_id = bi.id
                inner join account_move_line ml on ml.move_id = m.id
                inner join account_account a on a.id = ml.account_id
                    and a.reconcile = true
            ) c
            where c.import_id in %(ids)s
            group by c.import_id;
        """, {'ids': tuple(self.ids)})
        res = {}
        for rs in cr.fetchall():
            res[rs[0]] = {
                'count_move_line': rs[1],
                'count_unreconciled_move_line': rs[2],
                'count_no_partner_move_line': rs[3],
            }

        self._logger.debug("res = %s", res)
        for record in self:
            if record.id in res:
                record.count_move_line = res[record.id]['count_move_line']
                record.count_unreconciled_move_line = \
                    res[record.id]['count_unreconciled_move_line']
                record.count_no_partner_move_line = \
                    res[record.id]['count_no_partner_move_line']
            else:
                record.count_move_line = 0
                record.count_unreconciled_move_line = 0
                record.count_no_partner_move_line = 0

    @api.multi
    def unlink(self):
        # Allow to remove attachment and move if you can delete the import.
        for attachment in self.env['ir.attachment'].search([
            ('res_model', '=', self._name),
                ('res_id', 'in', self.ids)]):
            attachment.sudo().unlink()

        for record in self:
            for move in record.move_ids:
                move.unlink()

        return super(BvrImport, self).unlink()

    @api.multi
    def _get_file_hash(self):
        self.ensure_one()
        file_content = base64.decodestring(self.imported_file)
        return hashlib.sha256(file_content).hexdigest()

    @api.model
    def create(self, vals):
        with service_lock:
            bi = super(BvrImport, self).create(vals)
            if not bi.write_off_account_id:
                raise UserError(
                    _(u"The write-off account is not configured."))
            if not bi.date_field:
                raise UserError(
                    _(u"The ESR date field is not configured."))
            control_hash = bi._get_file_hash()

            # Remove existing import for this file if failed.
            self.search([
                ('control_hash', '=', control_hash),
                ('state', '=', 'failed')]).unlink()

            if self.search([
                    ('control_hash', '=', control_hash)], count=True):
                raise UserError(
                    _(u"This BVR file is already processed."))

            bi.control_hash = control_hash
            bi.queue_importation()
            return bi

    def queue_importation(self):
        self.ensure_one()
        self.state = 'queue_importation'
        self.queue_id = self.env['bss.queue'].new_job(
            _(u"Payments File Importation"), self._name,
            '_job_importation',
            '_job_importation_failed', self.id)

    @api.model
    def _job_importation(self, queue, bi_id):
        self.browse(bi_id)._importation(queue)

    @api.model
    def _job_importation_failed(self, queue, bi_id):
        self.invalidate_cache()
        bi = self.browse(bi_id)
        # To avoid user try to use the incomplete move and force him
        # to re-import the file, remove the linked move.
        if bi.move_ids:
            # The method button_cancel of account_move will do this query.
            # Copied here instead of calling button_cancel because
            # button_cancel will check if the journal allow move
            # cancellation. Here we want to cancel/delete in all cases.
            self.env.cr.execute("""
                update account_move
                set state=%s
                where id in %s;""", ('draft', tuple(bi.move_ids.ids)))
            try:
                bi.move_ids.unlink()
            except Exception:
                # Cannot happen because failed is not called in reconciliation
                # process.
                self._logger.exception("Impossible to remove linked moves")
        bi.state = 'terminated' if bi.state == 'imported' else 'failed'
        bi.message_post(body=_("The payment file importation process failed."))

    @api.multi
    def _importation(self, queue):
        self.ensure_one()
        qe_self = self.with_env(queue.env)
        qe_self.state = 'importation'
        qe_self.message_post(
            body=_(u"The importation process started."))
        queue.env.cr.commit()
        steps = queue.define_steps([
            (1, _(u"Importation")), (2, _(u"Reconciliation"))])
        try:
            first_line = cStringIO.StringIO(base64.decodestring(
                qe_self.imported_file)).readline()
            if first_line.startswith('<?xml'):
                self._process_camt054_file(steps[1])
            elif first_line.startswith('number,amount,esr_ref,date'):
                self._process_csv_file(steps[1])
            else:
                self._process_v11_file(steps[1])

            qe_self.message_post(
                body=_(u"The BVR import process finished successfully."))
            qe_self.state = 'imported'
        except TransactionRollbackError as e:
            self._logger.exception(e)
            self.env.cr.rollback()
            qe_self.message_post(body=_(
                u"The BVR import process failed due to concurrent "
                u"access error, please retry later."))
            raise
        except Exception as e:
            self._logger.exception(e)
            self.env.cr.rollback()
            raise
        finally:
            self.env.cr.commit()
            queue.env.cr.commit()

        # Auto start first reconciliation
        qe_self.state = 'reconciliation'
        qe_self.message_post(
            body=_(u"The reconciliation process started."))
        queue.env.cr.commit()
        self._reconciliation_step(steps[2])

    @api.multi
    def _process_csv_file(self, step):
        self.ensure_one()
        qe_self = self.with_env(step.queue_id.env)
        file_content = base64.decodestring(qe_self.imported_file)
        lines = cStringIO.StringIO(file_content).readlines()

        Invoice = self.env['account.invoice']

        qe_self.write({
            'transaction_type': '002',
            'line_count': len(lines) - 1,
            'payment_price_value': D('0')})
        qe_self.env.cr.commit()
        step.set_total(len(lines))
        hundredth = int(step.total / 100) + 1
        moves = {}
        total = D(0)
        for n, line in enumerate(lines):
            if n == 0:
                continue
            if not line.strip():
                continue
            self._logger.debug("Processing line %s", line)
            number, amount, ref, date_at = line.rstrip().split(',')
            total += D(amount)
            if n % hundredth == 0:
                self._logger.info(
                    "Create move lines:% 4d %%", 100 * n // step.total)
            inv = None
            if not ref and number:
                inv = Invoice.search([
                    ('number', '=', number)])
                if inv:
                    ref = inv.bss_esr_ref
            decoded_date = datetime.strptime(
                date_at, '%d.%m.%Y').date().isoformat()
            move_lines = self._get_move_line_data({
                'transaction_type': '002',
                'deposit_date': decoded_date,
                'treatment_date': decoded_date,
                'credit_date': decoded_date,
                'reference': ref,
                'amount': amount})
            if move_lines:
                for move_line in move_lines:
                    if not move_line['partner_id'] and inv:
                        move_line['partner_id'] = inv.partner_id
                    line_date = move_line['date']
                    if line_date not in moves:
                        moves[line_date] = [{
                            'date': line_date,
                            'amount': D(0),
                            'line_ids': []}]
                    elif len(moves[line_date][-1]['line_ids']) >= 100:
                        moves[line_date].append({
                            'date': line_date,
                            'amount': D(0),
                            'line_ids': []})
                    moves[line_date][-1]['amount'] += move_line.pop(
                        'opposite_amount')
                    moves[line_date][-1]['line_ids'].append((0, 0, move_line))
            step.set_current(n)
        qe_self.amount = float(total)
        qe_self.env.cr.commit()
        self._create_moves(moves)
        self.env.cr.commit()

    @api.multi
    def _process_camt054_file(self, step):
        self.ensure_one()
        qe_self = self.with_env(step.queue_id.env)
        file_content = base64.decodestring(qe_self.imported_file)
        parser = xml.sax.make_parser()
        parser.setFeature(xml.sax.handler.feature_namespaces, 0)
        Handler = Camt054Handler()
        parser.setContentHandler(Handler)
        parser.parse(cStringIO.StringIO(file_content))

        control = Handler.header
        lines = Handler.lines

        control['importation_date'] = datetime.today().isoformat()

        qe_self.write({
            'transaction_type': control['transaction_type'],
            'client_reference': control['client_reference'],
            'amount': control['amount'],
            'line_count': control['line_count'],
            'original_date': control['original_date'],
            'payment_price_value': control.get('payment_price_value', D('0'))})
        qe_self.env.cr.commit()
        step.set_total(len(lines))
        hundredth = int(step.total / 100) + 1
        moves = {}
        for n, payment in enumerate(lines, 1):
            if n % hundredth == 0:
                self._logger.info(
                    "Create move lines:% 4d %%", 100 * n // step.total)
            move_lines = self._get_move_line_data(payment)
            if move_lines:
                date_at = move_lines[0]['date']
                if date_at not in moves:
                    moves[date_at] = [{
                        'date': date_at,
                        'amount': D(0),
                        'line_ids': []}]
                elif len(moves[date_at][-1]['line_ids']) >= 100:
                    moves[date_at].append({
                        'date': date_at,
                        'amount': D(0),
                        'line_ids': []})
                for line in move_lines:
                    moves[date_at][-1]['amount'] += line.pop('opposite_amount')
                    moves[date_at][-1]['line_ids'].append((0, 0, line))
            step.set_current(n)
        self._create_moves(moves)
        self.env.cr.commit()

    @api.multi
    def _process_v11_file(self, step):
        self.ensure_one()
        qe_self = self.with_env(step.queue_id.env)
        file_content = base64.decodestring(self.imported_file)
        lines = cStringIO.StringIO(file_content).readlines()

        control_line = lines[-1].strip()
        if len(control_line) != 87:
            raise IOError("The V11 file is not valid.")
        control = {
            'transaction_type': control_line[0:3],
            'client_reference': control_line[3:12],
            'sort_key': control_line[12:39],
            'amount': D(control_line[39:51]) / D(100),
            'line_count': int(control_line[51:63]),
            'original_date':
                datetime.strptime(
                    control_line[63:69], '%y%m%d').isoformat(),
            'payment_price_value': D(control_line[69:78]) / D(100),
            'bvr_plus': control_line[78:87],
            'importation_date': datetime.today().isoformat(),
        }
        qe_self.write({
            'transaction_type': control['transaction_type'],
            'client_reference': control['client_reference'],
            'sort_key': control['sort_key'],
            'amount': control['amount'],
            'line_count': control['line_count'],
            'original_date': control['original_date'],
            'payment_price_value': control['payment_price_value'],
            'bvr_plus': control['bvr_plus']
        })
        qe_self.env.cr.commit()
        moves = {}
        step.set_total(len(lines) - 1)
        hundredth = int(step.total / 100) + 1
        for n, line in enumerate(lines[:-1]):
            if n % hundredth == 0:
                self._logger.info(
                    "Create move lines:% 4d %%", 100 * n // step.total)

            if len(line.strip()) != 100:
                raise IOError("The V11 file is not valid.")
            payment = {
                'transaction_type': line[0:3],
                'reference': line[12:39],
                'amount': D(line[39:49]) / D(100),
                'deposit_date': datetime.strptime(
                    line[59:65], '%y%m%d').isoformat(),
                'treatment_date': datetime.strptime(
                    line[65:71], '%y%m%d').isoformat(),
                'credit_date': datetime.strptime(
                    line[71:77], '%y%m%d').isoformat()
            }
            move_lines = self._get_move_line_data(payment)
            if move_lines:
                date_at = move_lines[0]['date']
                if date_at not in moves:
                    moves[date_at] = [{
                        'date': date_at,
                        'amount': D(0),
                        'line_ids': []}]
                elif len(moves[date_at][-1]['line_ids']) >= 100:
                    moves[date_at].append({
                        'date': date_at,
                        'amount': D(0),
                        'line_ids': []})
                for move_line in move_lines:
                    moves[date_at][-1]['amount'] += \
                        move_line.pop('opposite_amount')
                    moves[date_at][-1]['line_ids'].append((0, 0, move_line))
            step.set_current(n)
        self._create_moves(moves)
        self.env.cr.commit()

    @api.multi
    def _get_move_line_data(self, payment):
        self.ensure_one()
        MoveLine = self.env['account.move.line']

        if payment['transaction_type'] in (
                '002', '012', '005', '015', '102', '105', '112', '115'):
            debit = float(payment['transaction_type'] in
                          ('005', '015', '105', '115') and payment['amount'])
            credit = float(payment['transaction_type'] in
                           ('002', '012', '102', '112') and payment['amount'])

            res = []
            move_lines = MoveLine.search([
                ('bss_esr_ref', '=', payment['reference']),
                ('reconciled', '=', False),
                ('account_id.reconcile', '=', True),
                ('account_id.internal_type',
                 'in', ['receivable', 'payable'])], order='date')
            partner = move_lines.mapped('partner_id')
            if len(partner) > 1:
                self._logger.error(
                    "More than one partner on a ESR reference, "
                    "impossible to allocate.")
                return [{
                    'name': payment['reference'],
                    'date': payment[self.date_field],
                    'debit': float(debit),
                    'credit': float(credit),
                    'opposite_amount': D(credit) - D(debit),
                    'partner_id': None,
                    'account_id': self.debit_account_id.id,
                    'bss_esr_ref': payment['reference']}]
            for line in move_lines:
                if credit and line.amount_residual > 0:
                    if credit > line.amount_residual:
                        res.append({
                            'name': payment['reference'],
                            'date': payment[self.date_field],
                            'debit': float(debit),
                            'credit': float(line.amount_residual),
                            'opposite_amount':
                                D(line.amount_residual) - D(debit),
                            'partner_id': partner.id,
                            'account_id': self.debit_account_id.id,
                            'bss_esr_ref': payment['reference'],
                        })
                        credit -= line.amount_residual
                    else:
                        res.append({
                            'name': payment['reference'],
                            'date': payment[self.date_field],
                            'debit': float(debit),
                            'credit': float(credit),
                            'opposite_amount': D(credit) - D(debit),
                            'partner_id': partner.id,
                            'account_id': self.debit_account_id.id,
                            'bss_esr_ref': payment['reference'],
                        })
                        credit = 0
                elif debit and line.amount_residual < 0:
                    if debit > abs(line.amount_residual):
                        new_debit = abs(line.amount_residual)
                        res.append({
                            'name': payment['reference'],
                            'date': payment[self.date_field],
                            'debit': float(new_debit),
                            'credit': float(credit),
                            'opposite_amount':
                                D(credit) - D(new_debit),
                            'partner_id': partner.id,
                            'account_id': self.debit_account_id.id,
                            'bss_esr_ref': payment['reference'],
                        })
                        debit -= new_debit
                    else:
                        res.append({
                            'name': payment['reference'],
                            'date': payment[self.date_field],
                            'debit': float(debit),
                            'credit': float(credit),
                            'opposite_amount': D(credit) - D(debit),
                            'partner_id': partner.id,
                            'account_id': self.debit_account_id.id,
                            'bss_esr_ref': payment['reference'],
                        })
                        debit = 0
            if credit or debit:
                res.append({
                    'name': payment['reference'],
                    'date': payment[self.date_field],
                    'debit': float(debit),
                    'credit': float(credit),
                    'opposite_amount': D(credit) - D(debit),
                    'partner_id': partner.id,
                    'account_id': self.debit_account_id.id,
                    'bss_esr_ref': payment['reference']})
            return res
        else:
            self._logger.error("Not implemented transaction type %s", payment)
        return False

    @api.multi
    def _create_moves(self, moves):
        self.ensure_one()
        cpart_account_id = self.journal_id.default_credit_account_id.id
        move_list = []
        for m in moves.values():
            move_list.extend(m)
        nb = len(move_list)
        for cnt, move in enumerate(move_list, 1):
            f_amount = float(move['amount'])
            move_ref = _("Payments File Import %s/%s") % (
                cnt, nb) if nb > 1 else _("Payments File Import")
            self._logger.info(
                "Creating move for %s (%s/%s) %s", move['date'], cnt, nb,
                f_amount)
            move['line_ids'].append((0, 0, {
                'name': move_ref,
                'date': move['date'],
                'partner_id': None,
                'account_id': cpart_account_id,
                'debit': f_amount >= 0 and f_amount,
                'credit': f_amount < 0 and -f_amount,
            }))
            move_vals = {
                'bvr_import_id': self.id,
                'journal_id': self.journal_id.id,
                'date': move['date'],
                'line_ids': move['line_ids'],
                'ref': move_ref,
            }
            current = self.env['account.move'].create(move_vals)
            current.post()
            self._logger.info("Move %s created", cnt)

    def queue_reconciliation(self):
        self.ensure_one()
        if self.count_unreconciled_move_line < 1:
            raise UserError(_(u"There no move lines to reconcile."))
        self.state = 'queue_reconciliation'
        self.queue_id = self.env['bss.queue'].new_job(
            _(u"Payments File Reconciliation"), self._name,
            '_job_reconciliation',
            '_job_reconciliation_failed', self.id)

    @api.model
    def _job_reconciliation(self, queue, bi_id):
        self.browse(bi_id)._reconciliation(queue)

    @api.model
    def _job_reconciliation_failed(self, queue, bi_id):
        bi = self.browse(bi_id)
        bi.state = 'terminated'
        bi.message_post(body=_(
            u"The payment file reconciliation process failed."))

    @api.multi
    def _reconciliation(self, queue):
        self.ensure_one()
        qe_self = self.with_env(queue.env)
        qe_self.state = 'reconciliation'
        qe_self.message_post(
            body=_(u"The reconciliation process started."))
        queue.env.cr.commit()
        step = queue.define_steps([(1, _(u"Reconciliation"))])[1]
        self._reconciliation_step(step)

    @api.multi
    def _reconciliation_step(self, step):
        self.ensure_one()

        qe_self = self.with_env(step.queue_id.env)
        try:
            self._reconcile_move_lines(step)
            qe_self.message_post(
                body=_(u"The reconciliation process finished successfully."))
            qe_self.state = 'terminated'
        except TransactionRollbackError as e:
            self._logger.exception(e)
            self.env.cr.rollback()
            qe_self.message_post(body=_(
                u"The reconciliation failed due to concurrent "
                u"access error, please retry later."))
            raise
        except Exception as e:
            self._logger.exception(e)
            self.env.cr.rollback()
            raise
        finally:
            self.env.cr.commit()
            qe_self.env.cr.commit()

    @api.multi
    def _reconcile_move_lines(self, step):
        self.ensure_one()
        self.env.cr.execute("""
            select ml.bss_esr_ref
            from account_move_line ml
            inner join account_move m on
                ml.move_id = m.id
            inner join account_account a on
                ml.account_id = a.id
            where
                m.bvr_import_id = %s and
                a.reconcile = true and
                ml.bss_esr_ref is not null and
                ml.reconciled = false
            group by ml.bss_esr_ref;""", (self.id,))
        records = self.env.cr.fetchall()
        step.set_total(len(records))
        hundredth = int(step.total / 100) + 1
        for n, record in enumerate(records, 1):
            if n % hundredth == 0:
                self._logger.info(
                    "Reconcile move lines:% 4d %%", 100 * n // step.total)
            retry = 0
            while True:
                retry += 1
                try:
                    bvr_ref = record[0]
                    self.reconcile_ref(bvr_ref)
                except TransactionRollbackError:
                    if retry > 5:
                        raise
                    self._logger.warning(
                        "TransactionRollbackError, try %s", retry)
                    self.env.cr.rollback()
                    time.sleep(10)
                    continue
                break
            self.env.cr.commit()
            step.set_current(n)

    @api.model
    def reconcile_ref(self, bvr_ref, recursive=False):
        lock_date = self.env.user.company_id.account_lock_date()
        something_happened = False
        MoveLine = self.env['account.move.line']
        PartialReconcile = self.env['account.partial.reconcile']
        margin = self.reconcile_margin + .000001
        ml_args = [
            ('bss_esr_ref', '=', bvr_ref),
            ('reconciled', '=', False),
            ('account_id.reconcile', '=', True),
            ('account_id.internal_type', 'in', ['receivable', 'payable'])]
        debit_mls = MoveLine.search(
            ml_args + [('debit', '>', .0)], order='date, id')
        credit_mls = MoveLine.search(
            ml_args + [('credit', '>', .0)], order='date, id')

        partners = MoveLine.search([
            ('bss_esr_ref', '=', bvr_ref),
            ('account_id.reconcile', '=', True),
            ('account_id.internal_type', 'in', ['receivable', 'payable'])
        ]).filtered(lambda ml: ml.partner_id).mapped('partner_id')
        if len(partners) == 1:
            credit_mls.filtered(
                lambda ml: not ml.partner_id).write({
                    'partner_id': partners[0].id})
        else:
            self._logger.error(
                "More than one partner in invoices for ESR %s. Nothing can "
                "be reconciled.", bvr_ref)
            return
        if not debit_mls or not credit_mls:
            return

        if not recursive:
            # Take all linked lines by existing partial reconcile.
            # Set the bss_esr_ref if not equals to not loose the link
            # and remove partial reconcile to avoid conflicts
            # with this process.
            partials = PartialReconcile.sudo().search([
                ('full_reconcile_id', '=', False),
                '|', ('credit_move_id', 'in', credit_mls.ids),
                ('debit_move_id', 'in', debit_mls.ids)])
            debit_mls |= partials.mapped('debit_move_id')
            debit_mls = debit_mls.sorted(key=lambda l: (l.date, l.id))
            credit_mls |= partials.mapped('credit_move_id')
            credit_mls = credit_mls.sorted(key=lambda l: (l.date, l.id))
            (debit_mls | credit_mls).filtered(
                lambda l: l.bss_esr_ref != bvr_ref).write(
                    {'bss_esr_ref': bvr_ref})
            (debit_mls | credit_mls).remove_move_reconcile()

        credit_amount = .0
        credit_rec_mls = MoveLine
        # Take all credits until credit residual > first debit
        for credit_ml in credit_mls:
            credit_rec_mls |= credit_ml
            credit_amount += abs(credit_ml.amount_residual)
            if credit_amount >= (debit_mls[0].debit - margin):
                break
        if not credit_rec_mls:
            return
        debit_amount = .0
        debit_rec_mls = MoveLine
        # Take all debit as long as debit residual <= credit residual
        for debit_ml in debit_mls:
            if (debit_amount + abs(debit_ml.amount_residual) >
                    (credit_amount + margin)):
                break
            debit_rec_mls |= debit_ml
            debit_amount += abs(debit_ml.amount_residual)
        if not debit_rec_mls:
            # We can create a partial reconcile
            (debit_mls[0] | credit_mls[0]).reconcile(False, self.journal_id)
            return

        if (credit_amount > (debit_amount + margin) and
                orm2date(credit_rec_mls[-1].move_id.date) > lock_date):
            # If credit is greater, split last one to match debit amount
            ml1 = credit_rec_mls[-1].with_context(
                check_move_validity=False,
                skip_full_reconcile_check=True)
            ml1.move_id.sudo()._force_cancel()
            ml2 = ml1.copy()
            ml2.credit = credit_amount - debit_amount
            ml1.credit = ml1.credit - ml2.credit
            ml1.move_id.post()
        amount_total = 0.0
        rec_mls = debit_rec_mls | credit_rec_mls
        if len(rec_mls) > 1:
            write_off_account = False
            if abs(amount_total) < margin:
                write_off_account = self.write_off_account_id
            rec_mls.reconcile(write_off_account, self.journal_id)
            something_happened = True
        else:
            self._logger.debug(
                "Cannot reconcile %s %s", bvr_ref, rec_mls.ids)
        if something_happened:
            self.reconcile_ref(bvr_ref, recursive=True)
