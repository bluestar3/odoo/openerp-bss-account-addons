# -*- coding: utf-8 -*-
# Part of BVR Imports.
# See LICENSE file for full copyright and licensing details.

import contextlib
import logging
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen.canvas import Canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.units import mm, inch
import StringIO

from odoo import models, fields, api, _
from odoo.tools.misc import mod10r
from odoo.exceptions import UserError
from odoo.modules import get_module_resource
import odoo.addons.decimal_precision as dp  # @UnresolvedImport


class EsrPaymentSlip(models.Model):
    _name = 'bss_bvr_import.esr_payment_slip'
    _description = "ESR Payment Slip"
    _logger = logging.getLogger(_name)

    src_model = fields.Char("Source Model")
    src_id = fields.Integer("Source ID")
    partner_id = fields.Many2one(
        'res.partner', "Partner", required=True, ondelete='cascade')
    sender_partner_id = fields.Many2one(
        'res.partner', "Sender", required=True, ondelete='cascade')
    account_id = fields.Many2one(
        'account.account', "Account",
        domain=[('internal_type', '=', 'receivable')])
    bank_id = fields.Many2one(
        'account.journal', "Bank Account", required=True, domain=[
            ('type', '=', 'bank'),
            ('inbound_payment_method_ids.code', '=', 'esr')],
        default=lambda self: self.env.user.company_id.esr_bank_account_id.id)
    amount = fields.Float(string='Amount', digits=dp.get_precision('Account'))
    bss_esr_ref = fields.Char(string="ESR Reference", readonly=True)
    scan_line = fields.Char("ESR Scan Line", readonly=True)

    @api.multi
    def name_get(self):
        res = []
        for ps in self:
            res.append((ps.id, ps.bss_esr_ref or _("New ESR Reference")))
        return res

    @api.model
    def _generate_scan_line(self, bank, amount, esr_ref):
        if not bank or not bank.bvr_bank_adherent_num:
            self._logger.warning("No BVR adherent number")
            return ""
        if not esr_ref:
            return "{:s}>".format(bank.bvr_bank_adherent_num)
        if not amount:
            return "042>{:s}+ {:s}>".format(
                esr_ref, bank.bvr_bank_adherent_num)
        justified_amount = '01%s' % ('%.2f' % amount).replace(
            '.', '').rjust(10, '0')
        return "{:s}>{:s}+ {:s}>".format(
            mod10r(justified_amount), esr_ref, bank.bvr_bank_adherent_num)

    @api.model
    def create(self, vals):
        if not vals.get('sender_partner_id') and vals.get('partner_id'):
            vals['sender_partner_id'] = vals['partner_id']
        if vals.get('bank_id') and vals.get('partner_id'):
            bank = self.env['account.journal'].browse(vals['bank_id'])
            partner_ref = self.env['res.partner'].browse(
                vals['partner_id'])._get_esr_partner_ref()
            esr_ref = self._context.get(
                'force_esr_ref',
                bank._get_esr_ref(self.with_context(
                    esr_partner_ref=partner_ref).generate_payment_ref(
                        maxlength=bank._get_payment_ref_max_length())))
            vals.update({
                'bss_esr_ref': esr_ref,
                'scan_line': self._generate_scan_line(
                    bank, vals.get('amount'), esr_ref)})
        return super(EsrPaymentSlip, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'amount' not in vals:
            return True
        for ps in self:
            if ps.amount == vals['amount']:
                continue
            super(EsrPaymentSlip, ps).write({
                'amount': vals['amount'],
                'scan_line': self._generate_scan_line(
                    ps.bank_id, vals['amount'], ps.bss_esr_ref)})
        return True

    @api.model
    def generate_payment_ref(self, maxlength=26):
        """This method should return a unique reference to identify a
        debtor and an invoice. This reference is used to reconcile the invoice
        and the payment during the payment import process. The full bvr
        reference will be composed by concatenation of
        * the bvr_customer_adherent_num
        * this reference
        * a single digit as control number
        The default implementation uses a composition of the invoice_id and
        the partner_id as unique references. The returned reference should be
        exactly :maxlength characters long.
        """
        return self.env.user.company_id.esr_payment_ref_format.format(
            custom=self._context.get('esr_custom_ref', 0),
            type=self._context.get('esr_custom_type', 0),
            partner=self._context.get('esr_partner_ref', 0)
        ).rjust(maxlength, '0')

    @api.multi
    def get_formatted_esr_ref(self, use_space=True):
        """Returns the reference formated for printing in reference box
        (use_space = True) and receipt document (use_space = False). Only one
        invoice must be in self."""
        self.ensure_one()
        sep = use_space and " " or ""
        return sep.join([self.bss_esr_ref[::-1][i * 5:][:5]
                         for i in range(6)])[::-1].lstrip('0 ')

    @api.multi
    def _draw_recipient(self, canvas, dx=0.0):
        self.ensure_one()
        canvas.saveState()
        x = 0.2 * inch + dx
        y = 22.0 / 6.0 * inch
        address = canvas.beginText()
        address.setTextOrigin(x, y)
        address.setFont("Helvetica", 9)
        if self.bank_id.print_bank:
            address.textLine(self.bank_id.bank_id.name)
            address.textLine(("%s %s" % (
                self.bank_id.bank_id.zip,
                self.bank_id.bank_id.city)).strip())
            address.textLine('')
        address.textLine(self.env.user.company_id.partner_id.name)
        if self.env.user.company_id.partner_id.street:
            address.textLine(self.env.user.company_id.partner_id.street)
        if self.env.user.company_id.partner_id.street2:
            address.textLine(self.env.user.company_id.partner_id.street2)
        address.textLine(("%s %s" % (
            self.env.user.company_id.partner_id.zip,
            self.env.user.company_id.partner_id.city)).strip())
        canvas.drawText(address)
        canvas.restoreState()

    @api.multi
    def _draw_account(self, canvas, dx=0.0):
        self.ensure_one()
        canvas.saveState()
        x = 1.2 * inch + dx
        y = 14.0 / 6.0 * inch
        canvas.setFont("Helvetica", 10)
        canvas.drawString(x, y, self.bank_id.bvr_ccp_num)
        canvas.restoreState()

    @api.multi
    def _draw_amount(self, canvas, dx=0.0):
        self.ensure_one()
        if not self.amount:
            return
        canvas.saveState()
        x = 1.45 * inch + dx
        y = 12 / 6 * inch
        num_str, frac_str = ('{0:.2f}'.format(self.amount)).split('.')
        canvas.setFont('ocrb_font', 13)
        for ch in num_str[::-1]:
            canvas.drawString(x, y, ch)
            x -= 0.2 * inch
        x = 1.85 * inch + dx
        for ch in frac_str:
            canvas.drawString(x, y, ch)
            x += 0.2 * inch
        canvas.restoreState()

    @api.multi
    def _draw_sender_address(self, address):
        self.ensure_one()
        address.textLine(self.sender_partner_id.name.strip())
        if self.sender_partner_id.street:
            address.textLine(self.sender_partner_id.street)
        if self.sender_partner_id.street2:
            address.textLine(self.sender_partner_id.street2)
        address.textLine(
            ("%s %s" % (self.sender_partner_id.zip,
                        self.sender_partner_id.city)).strip())

    @api.multi
    def _draw_receipt_sender(self, canvas):
        self.ensure_one()
        canvas.saveState()
        x = 0.2 * inch
        y = 10.5 / 6.0 * inch
        address = canvas.beginText()
        address.setTextOrigin(x, y)
        address.setFont("Helvetica", 9)
        address.textLine(self.get_formatted_esr_ref(use_space=False))
        address.textLine('')
        self._draw_sender_address(address)
        canvas.drawText(address)
        canvas.restoreState()

    @api.multi
    def _draw_sender(self, canvas):
        self.ensure_one()
        canvas.saveState()
        x = 4.9 * inch
        y = 13.0 / 6.0 * inch
        address = canvas.beginText()
        address.setTextOrigin(x, y)
        address.setFont("Helvetica", 9)
        self._draw_sender_address(address)
        canvas.drawText(address)
        canvas.restoreState()

    @api.multi
    def _draw_bvr_reference(self, canvas):
        self.ensure_one()
        if not self.bss_esr_ref:
            return
        x = 7.9 * inch
        y = 16.0 / 6.0 * inch
        canvas.setFont('ocrb_font', 13)
        for ch in self.get_formatted_esr_ref()[::-1]:
            canvas.drawString(x, y, ch)
            x -= 0.1 * inch

    @api.multi
    def _draw_bvr_scan_line(self, canvas):
        self.ensure_one()
        if not self.scan_line and self.bss_esr_ref:
            self.scan_line = self._get_new_esr_scan_line(self.bss_esr_ref)
        if not self.scan_line:
            return
        canvas.saveState()
        x = 7.8 * inch
        y = 4.0 / 6.0 * inch
        canvas.setFont('ocrb_font', 13)
        for ch in self.scan_line[::-1]:
            canvas.drawString(x, y, ch)
            x -= 0.1 * inch
        canvas.restoreState()

    @api.multi
    def _draw_bvr(self, canvas):
        self.ensure_one()
        canvas.saveState()
        canvas.translate(self.env.user.company_id.esr_delta_horizontal * mm,
                         self.env.user.company_id.esr_delta_vertical * mm)
        self._draw_recipient(canvas)
        self._draw_account(canvas)
        self._draw_amount(canvas)
        self._draw_receipt_sender(canvas)
        dx = 2.4 * inch
        self._draw_recipient(canvas, dx)
        self._draw_account(canvas, dx)
        self._draw_amount(canvas, dx)
        self._draw_bvr_reference(canvas)
        self._draw_sender(canvas)
        self._draw_bvr_scan_line(canvas)
        canvas.restoreState()

    @api.multi
    def _draw_pdf_report(self, report_name):
        self.ensure_one()
        pdfmetrics.registerFont(TTFont(
            'ocrb_font',
            get_module_resource(
                'bss_bvr_import', 'static', 'src', 'font', 'ocrbb.ttf'
            )
        ))
        with contextlib.closing(StringIO.StringIO()) as buff:
            canvas = Canvas(buff, A4, pageCompression=None)

            if report_name == 'bss_bvr_import.esr_payment_slip':
                self._draw_bvr(canvas)
                canvas.save()
                return buff.getvalue()
            else:
                res = super(EsrPaymentSlip, self)._draw_pdf_report(report_name)
                if not res:
                    raise UserError(
                        _("No reportlab-pdf definition for report %s") %
                        report_name
                    )
                else:
                    return res
