# -*- coding: utf-8 -*-
# Part of BVR Imports.
# See LICENSE file for full copyright and licensing details.

import contextlib
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen.canvas import Canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.units import mm, inch
import StringIO

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.netsvc import logging
from odoo.modules import get_module_resource


class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    _logger = logging.getLogger(_inherit)

    esr_payment_slip_id = fields.Many2one(
        'bss_bvr_import.esr_payment_slip', "ESR Payment Slip",
        ondelete='set null', copy=False)
    bss_esr_ref = fields.Char(
        "ESR Reference", readonly=True, copy=False)
    scan_line = fields.Char(
        related='esr_payment_slip_id.scan_line', readonly=True)

    @api.model
    def create(self, vals):
        inv = super(AccountInvoice, self).create(vals)
        return inv

    @api.multi
    def invoice_validate(self):
        esr_bank_account = (
            self.env.user.company_id.esr_bank_account_id)
        for inv in self:
            if (not inv.esr_payment_slip_id and inv.type == 'out_invoice' and
                    esr_bank_account):
                inv.partner_bank_id = esr_bank_account.bank_account_id

        res = super(AccountInvoice, self).invoice_validate()

        PaymentSlip = self.env['bss_bvr_import.esr_payment_slip']
        bank = self.company_id.esr_bank_account_id
        for inv in self:
            if inv.type == 'out_invoice':
                if not inv.bss_esr_ref:
                    partner_ref = self.partner_id._get_esr_partner_ref()
                    inv.bss_esr_ref = bank._get_esr_ref(
                        PaymentSlip.with_context(
                            esr_partner_ref=partner_ref,
                            esr_custom_ref=self._get_esr_invoice_ref(),
                            esr_custom_type=self.company_id.esr_invoice_type
                        ).generate_payment_ref(
                            maxlength=bank._get_payment_ref_max_length()))
                if inv.esr_payment_slip_id:
                    inv.esr_payment_slip_id.write({'amount': inv.amount_total})
                inv.move_id._set_esr_ref(inv.bss_esr_ref)
        return res

    @api.multi
    def _get_esr_invoice_ref(self):
        self.ensure_one()
        return self.company_id._get_esr_invoice_ref(self)

    @api.multi
    def _get_esr_payment_split(self):
        self.ensure_one()
        esr_bank_account = self.company_id.esr_bank_account_id
        if self.esr_payment_slip_id:
            return self.esr_payment_slip_id
        elif self.type == 'out_invoice' and esr_bank_account:
            PaymentSplip = self.env['bss_bvr_import.esr_payment_slip']
            self.esr_payment_slip_id = PaymentSplip.with_context(
                force_esr_ref=self.bss_esr_ref).create({
                    'src_model': self._name,
                    'src_id': self.id,
                    'partner_id': self.commercial_partner_id.id,
                    'account_id': self.account_id.id,
                    'bank_id': esr_bank_account.id,
                    'amount': self.amount_total})
        return self.esr_payment_slip_id

    @api.multi
    def _draw_bvr(self, canvas):
        self.ensure_one()
        if self.state == 'draft':
            raise UserError(_(u"You cannot print an ESR on draft invoice!"))
        if not self.bss_esr_ref:
            raise UserError(
                _(u"You cannot print an ESR on invoice with no reference!"))
        canvas.saveState()
        canvas.translate(self.env.user.company_id.esr_delta_horizontal * mm,
                         self.env.user.company_id.esr_delta_vertical * mm)
        ps = self._get_esr_payment_split()
        ps._draw_recipient(canvas)
        ps._draw_account(canvas)
        ps._draw_amount(canvas)
        ps._draw_receipt_sender(canvas)
        dx = 2.4 * inch
        ps._draw_recipient(canvas, dx)
        ps._draw_account(canvas, dx)
        ps._draw_amount(canvas, dx)
        ps._draw_bvr_reference(canvas)
        ps._draw_sender(canvas)
        ps._draw_bvr_scan_line(canvas)
        canvas.restoreState()

    @api.multi
    def _draw_pdf_report(self, report_name):
        self.ensure_one()
        pdfmetrics.registerFont(TTFont(
            'ocrb_font',
            get_module_resource(
                'bss_bvr_import', 'static', 'src', 'font', 'ocrbb.ttf')))
        with contextlib.closing(StringIO.StringIO()) as buff:
            canvas = Canvas(buff, A4, pageCompression=None)

            if report_name == 'bss_bvr_import.bss_bvr':
                self._draw_bvr(canvas)
                canvas.save()
                return buff.getvalue()
            else:
                res = super(AccountInvoice, self)._draw_pdf_report(report_name)
                if not res:
                    raise UserError(
                        _("No reportlab-pdf definition for report %s") %
                        report_name
                    )
                else:
                    return res
