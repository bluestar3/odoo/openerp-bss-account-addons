# -*- coding: utf-8 -*-
# Part of BVR Imports.
# See LICENSE file for full copyright and licensing details.
from odoo import models, api
from odoo.netsvc import logging
import StringIO
import pyPdf


class Report(models.Model):

    _inherit = 'report'
    _logger = logging.getLogger(_inherit)

    @api.multi
    def _generate_report(self, report_name, model):
        ReportModel = self.env[model]

        docs = ReportModel.browse(self.ids)
        if len(docs) == 1:
            return docs[0]._draw_pdf_report(report_name)
        else:
            pdfs = (x._draw_pdf_report(report_name) for x in docs)
            return self.merge_pdf_in_memory(pdfs)

    @api.multi
    def get_pdf(self, docids, report_name, html=None, data=None):
        self.env.cr.execute(
            "SELECT * FROM ir_act_report_xml WHERE report_name=%s",
            (report_name,))
        report = self.env.cr.dictfetchone()
        if report and report['report_type'] == 'reportlab-pdf':
            reports = self.browse(docids)
            return reports._generate_report(report_name=report_name,
                                            model=report['model'])
        else:
            return super(Report, self).get_pdf(docids, report_name, html=html,
                                               data=data)

    def merge_pdf_in_memory(self, docs):
        streams = []
        writer = pyPdf.PdfFileWriter()
        for doc in docs:
            current_buff = StringIO.StringIO()
            streams.append(current_buff)
            current_buff.write(doc)
            current_buff.seek(0)
            reader = pyPdf.PdfFileReader(current_buff)
            for page in xrange(reader.getNumPages()):
                writer.addPage(reader.getPage(page))
        buff = StringIO.StringIO()
        try:
            # The writer close the reader file here
            writer.write(buff)
            return buff.getvalue()
        except IOError:
            raise
        finally:
            buff.close()
            for stream in streams:
                stream.close()
