# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* bss_pain_export
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-02-06 13:27+0000\n"
"PO-Revision-Date: 2020-02-06 13:27+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: bss_pain_export
#: model:ir.ui.view,arch_db:bss_pain_export.account_invoice_supplier_form_view_inherit_bss_pain_export
msgid "A valid bank account cannot be determined automatically.\n"
"                    This invoice cannot be selected for Pain Export.\n"
"                    Please select a valid bank account or fill the ESR fields\n"
"                    to enable it."
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_account_balance
msgid "Account Balance"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:335
#: code:addons/bss_pain_export/models/payment_process.py:344
#, python-format
msgid "Action only available on payment process in state 'draft'"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:364
#: code:addons/bss_pain_export/models/payment_process.py:373
#, python-format
msgid "Action only available on payment process in state 'exported'"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:318
#, python-format
msgid "Already paid"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:347
#, python-format
msgid "Already paid line %s"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_amount_residual
msgid "Amount Residual"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_amount_to_pay
msgid "Amount To Pay"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_bank_account_id
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_journal_id
msgid "Bank Account"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_account_invoice_pain_partner_bank_id
msgid "Bank Account for Pain Export"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:314
#, python-format
msgid "Cannot change line state of validated payment process."
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:201
#, python-format
msgid "Cannot delete a non draft process"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_communication
msgid "Communication"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_iban_format_create_uid
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_create_uid
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_create_uid
msgid "Created by"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_iban_format_create_date
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_create_date
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_create_date
msgid "Created on"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_date_maturity
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_date_maturity
msgid "Date Maturity"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_date_paid
msgid "Date Paid"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_date_value
msgid "Date Value"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_iban_format_display_name
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_display_name
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_display_name
msgid "Display Name"
msgstr ""

#. module: bss_pain_export
#: selection:bss_pain_export.payment_process,state:0
#: model:ir.ui.view,arch_db:bss_pain_export.bss_pain_export_payment_process_view_search
msgid "Draft"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:194
#, python-format
msgid "Draft Export"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_esr_adherent_number
#: model:ir.ui.view,arch_db:bss_pain_export.account_invoice_supplier_form_view_inherit_bss_pain_export
msgid "ESR Adherent Number"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_bss_esr_ref
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_bss_esr_ref_digits
#: model:ir.ui.view,arch_db:bss_pain_export.account_invoice_supplier_form_view_inherit_bss_pain_export
msgid "ESR Reference"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/account_invoice.py:39
#, python-format
msgid "ESR adherent number control digit is invalid."
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/account_invoice.py:57
#, python-format
msgid "ESR adherent number is required for ESR payments."
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/account_invoice.py:42
#, python-format
msgid "ESR adherent number must be entered as XX-XXXXXX-X."
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/account_invoice.py:54
#, python-format
msgid "ESR reference control digit is invalid."
msgstr ""

#. module: bss_pain_export
#: model:ir.ui.view,arch_db:bss_pain_export.bss_pain_export_payment_process_view_form
msgid "Export"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:113
#, python-format
msgid "Export File"
msgstr ""

#. module: bss_pain_export
#: selection:bss_pain_export.payment_process,state:0
#: model:ir.ui.view,arch_db:bss_pain_export.bss_pain_export_payment_process_view_search
msgid "Exported"
msgstr ""

#. module: bss_pain_export
#: model:ir.actions.act_window,name:bss_pain_export.bss_iban_format_action
#: model:ir.ui.menu,name:bss_pain_export.bss_iban_format_menu
msgid "IBAN Formats"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_iban_format_id
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_id
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_id
msgid "ID"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_iban_format_institution_id_regex
msgid "Institution ID"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:251
#, python-format
msgid "Invalid bank account."
msgstr ""

#. module: bss_pain_export
#: model:ir.model,name:bss_pain_export.model_account_invoice
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_invoice_id
msgid "Invoice"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_iban_format___last_update
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line___last_update
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process___last_update
msgid "Last Modified on"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_iban_format_write_uid
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_write_uid
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_write_uid
msgid "Last Updated by"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_iban_format_write_date
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_write_date
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_write_date
msgid "Last Updated on"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:354
#, python-format
msgid "Line %s already selected in another payment process"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:327
#, python-format
msgid "Line already selected in another payment process"
msgstr ""

#. module: bss_pain_export
#: model:ir.actions.server,name:bss_pain_export.action_payment_line_not_selected
msgid "Lines Not Selected"
msgstr ""

#. module: bss_pain_export
#: model:ir.actions.server,name:bss_pain_export.action_payment_line_paid
msgid "Lines Paid"
msgstr ""

#. module: bss_pain_export
#: model:ir.actions.server,name:bss_pain_export.action_payment_line_rejected
msgid "Lines Rejected"
msgstr ""

#. module: bss_pain_export
#: model:ir.actions.server,name:bss_pain_export.action_payment_line_selected
msgid "Lines Selected"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/account_invoice.py:51
#, python-format
msgid "Maximum ESR reference length is 27 digits."
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:399
#, python-format
msgid "Missing bank identification for line %s"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:257
#, python-format
msgid "Missing debitor bank identification"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_move_line_id
msgid "Move Line"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_name
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_name
msgid "Name"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:42
#, python-format
msgid "New Export"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:221
#, python-format
msgid "No lines to export!"
msgstr ""

#. module: bss_pain_export
#: selection:bss_pain_export.payment_line,selected:0
#: model:ir.ui.view,arch_db:bss_pain_export.bss_pain_export_payment_lines_view_search
msgid "Not Selected"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:403
#, python-format
msgid "Not implemented payment type for line %s"
msgstr ""

#. module: bss_pain_export
#: model:ir.ui.view,arch_db:bss_pain_export.bss_pain_export_payment_process_view_search
msgid "Open"
msgstr ""

#. module: bss_pain_export
#: selection:bss_pain_export.payment_line,paid:0
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_paid
#: model:ir.ui.view,arch_db:bss_pain_export.bss_pain_export_payment_lines_view_search
msgid "Paid"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_paid_amount
msgid "Paid Amount"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_pain_valid
msgid "Pain Valid"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_partner_id
#: model:ir.ui.view,arch_db:bss_pain_export.bss_pain_export_payment_lines_view_search
msgid "Partner"
msgstr ""

#. module: bss_pain_export
#: model:ir.actions.act_window,name:bss_pain_export.bss_pain_export_payment_process_action
#: model:ir.ui.menu,name:bss_pain_export.bss_pain_export_payment_process_menu
msgid "Payment Exports"
msgstr ""

#. module: bss_pain_export
#: model:ir.actions.act_window,name:bss_pain_export.bss_pain_export_payment_lines_action
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_count_payment_lines
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_payment_line_ids
#: model:ir.ui.view,arch_db:bss_pain_export.bss_pain_export_payment_process_view_form
msgid "Payment Lines"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_payment_process_id
msgid "Payment Process"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:131
#, python-format
msgid "Please fill in the Date Paid to validate."
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:103
#, python-format
msgid "Please fill in the Date Value to export."
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_iban_format_prefix
msgid "Prefix"
msgstr ""

#. module: bss_pain_export
#: selection:bss_pain_export.payment_line,paid:0
#: model:ir.ui.view,arch_db:bss_pain_export.bss_pain_export_payment_lines_view_search
msgid "Rejected"
msgstr ""

#. module: bss_pain_export
#: selection:bss_pain_export.payment_line,selected:0
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_selected
#: model:ir.ui.view,arch_db:bss_pain_export.bss_pain_export_payment_lines_view_search
msgid "Selected"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_line_payment_process_state
#: model:ir.model.fields,field_description:bss_pain_export.field_bss_pain_export_payment_process_state
msgid "State"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_account_invoice_supplier_esr_adherent_number
msgid "Supplier ESR Adherent Number"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_account_invoice_supplier_esr_ref
msgid "Supplier ESR Reference"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_account_invoice_supplier_esr_ref_digits
msgid "Supplier ESR Reference (without spaces)"
msgstr ""

#. module: bss_pain_export
#: model:ir.ui.view,arch_db:bss_pain_export.view_partner_bank_form_inherit_bss_pain_export
msgid "Technical Type"
msgstr ""

#. module: bss_pain_export
#: model:ir.model.fields,field_description:bss_pain_export.field_account_invoice_pain_valid
msgid "Valid for Pain Export"
msgstr ""

#. module: bss_pain_export
#: model:ir.ui.view,arch_db:bss_pain_export.bss_pain_export_payment_process_view_form
msgid "Validate"
msgstr ""

#. module: bss_pain_export
#: selection:bss_pain_export.payment_process,state:0
msgid "Validated"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:314
#: code:addons/bss_pain_export/models/payment_process.py:318
#: code:addons/bss_pain_export/models/payment_process.py:327
#, python-format
msgid "Warning"
msgstr ""

#. module: bss_pain_export
#: model:ir.ui.view,arch_db:bss_pain_export.account_invoice_supplier_form_view_inherit_bss_pain_export
msgid "XX-XXXXXX-X"
msgstr ""

#. module: bss_pain_export
#: code:addons/bss_pain_export/models/payment_process.py:99
#, python-format
msgid "You cannot select a line without valid bank account."
msgstr ""

#. module: bss_pain_export
#: model:ir.model,name:bss_pain_export.model_bss_iban_format
msgid "bss.iban_format"
msgstr ""

#. module: bss_pain_export
#: model:ir.model,name:bss_pain_export.model_bss_pain_export_payment_line
msgid "bss_pain_export.payment_line"
msgstr ""

#. module: bss_pain_export
#: model:ir.model,name:bss_pain_export.model_bss_pain_export_payment_process
msgid "bss_pain_export.payment_process"
msgstr ""

#. module: bss_pain_export
#: model:account.payment.method,name:bss_pain_export.account_payment_method_pain
msgid "pain.001"
msgstr ""

