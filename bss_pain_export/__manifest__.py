# -*- coding: utf-8 -*-
# Part of Pain Exports.
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Pain Exports',
    'version': '10.0.4.7.0',
    "category": 'Bluestar/Generic module',
    'complexity': "easy",
    'description':
    """A module to export pain files""",
    'author': u'Bluestar Solutions Sàrl',
    'website': 'http://www.bluestar.solutions',
    'depends': [
        'account',
        'base_iban',
        'bss_utils',
    ],
    'data': [
        'security/ir.model.access.csv',

        'data/account.payment.method.xml',
        'data/bss_iban_format_data.xml',

        'views/bss_iban_format_views.xml',
        'views/account_invoice_views.xml',
        'views/payment_process_views.xml',
        'views/res_bank_views.xml',
    ],
    'demo': [],
    'test': [],
    'installable': True,
    'application': False,
    'auto_install': False,
}
