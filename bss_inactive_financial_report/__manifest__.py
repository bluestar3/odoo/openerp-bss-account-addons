# -*- coding: utf-8 -*-
# Part of Inactive Financial Report.
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Inactive Financial Report',
    'version': '10.0.4.7.0',
    "category": 'Bluestar/Generic module',
    'complexity': "easy",
    'description': """
Inactive Financial Report
=========================

Allow to inactivate financial report. Usefull to avoid not used
financial report to be created each times by addons update.
    """,
    'author': 'Bluestar Solutions Sàrl',
    'website': 'https://bluestar.solutions',
    'depends': ['account'],
    'data': ['views/account_financial_report_views.xml'],
    'installable': True,
    'application': False,
    'auto_install': False,
}
