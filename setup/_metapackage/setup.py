import setuptools

with open('VERSION.txt', 'r') as f:
    version = f.read().strip()

setuptools.setup(
    name="odoo10-addons-odoo10-addons-bss-account",
    description="Meta package for odoo10-addons-bss-account Odoo addons",
    version=version,
    install_requires=[
        'odoo10-addon-bss_bvr_import',
        'odoo10-addon-bss_fix_account_report',
        'odoo10-addon-bss_inactive_financial_report',
        'odoo10-addon-bss_pain_export',
    ],
    classifiers=[
        'Programming Language :: Python',
        'Framework :: Odoo',
    ]
)
